package com.qmetry.qaf.example.listeners;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DriverCommand;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.CommandTracker;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElementCommandListener;
import com.qmetry.qaf.example.steps.CommonUtils;

public class Listeners extends WebDriverTestBase  implements QAFWebElementCommandListener {

	public void beforeCommand(QAFExtendedWebElement element, CommandTracker commandTracker) {
		// TODO Auto-generated method stub
		
	}

	public void afterCommand(QAFExtendedWebElement element, CommandTracker commandTracker) {
		if (commandTracker.getCommand().equalsIgnoreCase(DriverCommand.CLICK_ELEMENT)) {
			QAFTestBase.pause(12000);
		//	waitForNotVisible("loader.wait");
			
			 } 
		if(commandTracker.getCommand().equalsIgnoreCase(DriverCommand.SEND_KEYS_TO_ELEMENT))
		{
			QAFTestBase.pause(3000);
			//waitForNotVisible("loader.wait");
		}
		
		
	}

	public void onFailure(QAFExtendedWebElement element, CommandTracker commandTracker) {
		// TODO Auto-generated method stub
		
	}

}
