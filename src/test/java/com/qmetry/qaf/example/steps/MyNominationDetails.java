package com.qmetry.qaf.example.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class MyNominationDetails extends WebDriverTestBase {

	ArrayList<String> breadcrumbtext = new ArrayList<String>();
	public  void checkMyNominations()
	{
		click("side.menu");
		CommonUtils.scrollUpToElement("RnR.menu");
		click("RnR.menu");
		CommonUtils.scrollUpToElement("emp.myrnr.menu");
		click("emp.myrnr.menu");
		CommonUtils.scrollUpToElement("emp.mynominations.menu");
		click("emp.mynominations.menu");
	}

	public   void checkTitle() {
		boolean isTitlePresent = verifyPresent("emp.mynomination.title");
		Assert.assertTrue(isTitlePresent);

	}

	public  void checkBredcrum() {
		String crum;
		List<QAFWebElement> list = getDriver().findElements("emp.mynomination.breadcrum");
		for (QAFWebElement ele : list) {
			breadcrumbtext.add(ele.getText());
		}
		crum = breadcrumbtext.get(0).concat(breadcrumbtext.get(1));
		boolean checkBreadcrum = Validator.verifyThat("Check breadcrum",crum , Matchers.containsString("Home / R & R /  My R & R"));
		Assert.assertTrue(checkBreadcrum);
	} 
	public void checkColumns()
	{
		boolean isNomineePresent = verifyPresent("emp.nominees.label");
		boolean isNoofRewardsPresent = verifyPresent("emp.noofrewards.label");
		boolean isManagerStatusPresent = verifyPresent("emp.managerstatus.label");
		boolean isHrStatusPresent = verifyPresent("emp.hrstatus.label");
		SoftAssert sa=new SoftAssert();
		sa.assertTrue(isNomineePresent);

		sa.assertTrue(isNoofRewardsPresent);

		sa.assertTrue(isManagerStatusPresent);

		sa.assertTrue(isHrStatusPresent);

		sa.assertAll();


	}

	public void checkPlusSymbol() 
	{
		if(Integer.parseInt(getText("emp.noofrewards.value"))>1) 
		{
			boolean isPlusbtnPresent = verifyPresent("emp.plus.btn");
			Assert.assertTrue(isPlusbtnPresent);
		}
	}
	
	public void checkPaginationLink()
	{
		boolean isPaginationPresent = verifyPresent("emp.pagination.link");
		Assert.assertTrue(isPaginationPresent);
	
	
} 

}
