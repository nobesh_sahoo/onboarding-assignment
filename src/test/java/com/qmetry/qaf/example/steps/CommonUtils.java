package com.qmetry.qaf.example.steps;


import static com.qmetry.qaf.automation.step.CommonStep.*;
import org.openqa.selenium.JavascriptExecutor;


import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

public class CommonUtils extends WebDriverTestBase {
	public static final String LOADER = "wait.loader";

	public static void clickUsingJavascript(String loc) {
		waitForVisible(loc);
		JavascriptExecutor js =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		js.executeScript("arguments[0].click();", new QAFExtendedWebElement(loc));
		QAFTestBase.pause(12000);
	}

	public static void scrollUpToElement(String ele) {
		JavascriptExecutor executor =
				(JavascriptExecutor) new WebDriverTestBase().getDriver();
		executor.executeScript("arguments[0].scrollIntoView(true)",
				new QAFExtendedWebElement(ele));
	}

	public static boolean verifyLocContainsText(String loc, String text) {
		return new QAFExtendedWebElement(loc).getText().contains(text);
	}

	public static void switchToActiveElement()
	{
		new WebDriverTestBase().getDriver().switchTo().activeElement();
	}

	public static void login(String username, String password)
	{
		waitForPresent("uname.txtfield");
		sendKeys(username, "uname.txtfield");
		sendKeys(password, "pwd.txtfield");
		click("login.btn");
	}
	@QAFTestStep(description ="user maximises window")
	public void maximiseWindow()
	{
		getDriver().manage().window().maximize();
	}

	@QAFTestStep(description = "user clicks on logout")
	public static void logout()
	{
		waitForPresent("logout.btn");
		CommonUtils.clickUsingJavascript("logout.btn");
	}
	public static void waitForLoader(){
		waitForNotVisible(LOADER,60);
	}


}
