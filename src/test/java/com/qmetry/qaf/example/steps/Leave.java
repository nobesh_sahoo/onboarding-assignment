package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.awt.AWTException;

import java.util.Random;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;


import com.qmetry.qaf.automation.step.QAFTestStep;

public class Leave  extends WebDriverTestBase{
	private static SoftAssert soft;
	private static String monthyear="-October-2018";
	@QAFTestStep(description="user selects side navigation tab->Leave->Apply Leave")
	public void applyLeaveForOneDay() throws AWTException
	{
		click("side.menu");
		click("homepage.leave.menu");
		click("homepage.applyleave.menu");
		click("leavepage.fromdate.textfield");
		getDriver().findElement("leavepage.fromdate.textfield").clear();

		Random ran=new Random();
		int leavedd = 1+ ran.nextInt(29);
		String date = String.format("%02d", leavedd);
		String leavedate = date.concat(monthyear);		
		sendKeys(leavedate, "leavepage.fromdate.textfield");
		click("leavepage.reasonforleave.drpdwn");
		CommonUtils.scrollUpToElement("leavepage.reasonforleave.drpdwnvalue(oneday)");
		click("leavepage.reasonforleave.drpdwnvalue(oneday)");
		click("leavepage.todate.textfield");
		getDriver().findElement("leavepage.todate.textfield").clear();
		sendKeys(leavedate, "leavepage.todate.textfield");
		click("leavepage.applyleave.button");
	}
	@QAFTestStep(description="user clicks on select side navigation tab->Leave->Apply Leave inluding holidays")
	public void applyLeaveIncludingHolidays() throws AWTException
	{
		click("side.menu");
		click("homepage.leave.menu");
		click("homepage.applyleave.menu");
		click("leavepage.fromdate.textfield");
		getDriver().findElement("leavepage.fromdate.textfield").clear();
		Random ran=new Random();

		int leavefromdd = 1+ ran.nextInt(4);
		String fromdate = String.format("%02d", leavefromdd);
		String leavefromdate = fromdate.concat(monthyear);		
		sendKeys(leavefromdate, "leavepage.fromdate.textfield");
		click("leavepage.reasonforleave.drpdwn");
		CommonUtils.scrollUpToElement("leavepage.reasonforleave.drpdwnvalue(multipledays)");
		click("leavepage.reasonforleave.drpdwnvalue(multipledays)");
		click("leavepage.todate.textfield");
		getDriver().findElement("leavepage.todate.textfield").clear();
		int leavetodd=7+leavefromdd;
		String todate = String.format("%02d", leavetodd);
		String leavetodate = todate.concat(monthyear);	
		sendKeys(leavetodate, "leavepage.todate.textfield");

		QAFExtendedWebElement qafleavevalue = new QAFExtendedWebElement("leavepage.total.value");
		boolean verify = Validator.verifyThat("6", qafleavevalue.getText(),
				Matchers.containsString("6"));
		Assert.assertTrue(verify);
		click("leavepage.applyleave.button");

	}

	@QAFTestStep(description="user clicks on side navigation tab->Leave->Team Leave List")
	public void teamLeaveList()
	{
		click("side.menu");
		click("homepage.mngr.leave.menu");
		click("homepage.mngr.teamleavelist.menu");
	}

	@QAFTestStep(description="user verifies columns")
	public void teamLeaveListColumnVerification()
	{
		QAFExtendedWebElement enamelbl = new QAFExtendedWebElement("leavelistpage.ename.lbl");

		QAFExtendedWebElement eidlbl = new QAFExtendedWebElement("leavelistpage.eid.lbl");

		QAFExtendedWebElement etypelbl = new QAFExtendedWebElement("leavelistpage.etype.lbl");

		QAFExtendedWebElement ldurationlbl = new QAFExtendedWebElement("leavelistpage.lduration.lbl");

		QAFExtendedWebElement statuslbl = new QAFExtendedWebElement("leavelistpage.status.lbl");

		QAFExtendedWebElement reasonlbl = new QAFExtendedWebElement("leavelistpage.reason.lbl");

		QAFExtendedWebElement projectlbl = new QAFExtendedWebElement("leavelistpage.project.lbl");

		QAFExtendedWebElement mngrcmmntlbl = new QAFExtendedWebElement("leavelistpage.mngrcmmnt.lbl");

		QAFExtendedWebElement actionslbl = new QAFExtendedWebElement("leavelistpage.actions.lbl");

		QAFExtendedWebElement backdatedlbl = new QAFExtendedWebElement("leavelistpage.backdated.lbl");

		QAFExtendedWebElement applieddatelbl = new QAFExtendedWebElement("leavelistpage.applieddate.lbl");

		QAFExtendedWebElement leavedatelbl = new QAFExtendedWebElement("leavelistpage.leavedate.lbl");

		soft=new SoftAssert();

		boolean verifyenamelbl = Validator.verifyThat("Employee Name", enamelbl.getText(),
				Matchers.containsString("Name"));
		boolean verifyeidlbl = Validator.verifyThat("Employee Name (ID)", eidlbl.getText(),
				Matchers.containsString("ID"));
		boolean verifyetypelbl = Validator.verifyThat("Type", etypelbl.getText(),
				Matchers.containsString("Type"));
		boolean verifyldurationlbl = Validator.verifyThat("Leave Duration ", ldurationlbl.getText(),
				Matchers.containsString("Duration"));
		boolean verifystatuslbl = Validator.verifyThat("Status ", statuslbl.getText(),
				Matchers.containsString("Status"));
		boolean verifyreasonlbl = Validator.verifyThat("Leave Reason ", reasonlbl.getText(),
				Matchers.containsString("Reason"));
		boolean verifyprojectlbl = Validator.verifyThat("Project", projectlbl.getText(),
				Matchers.containsString("Project"));
		boolean verifymngrcmmntlbl = Validator.verifyThat("Manager's Comment ", mngrcmmntlbl.getText(),
				Matchers.containsString("Comment"));
		boolean verifyactionslbl = Validator.verifyThat("Actions", actionslbl.getText(),
				Matchers.containsString("Actions"));
		boolean verifybackdatedlbl = Validator.verifyThat(" Back Dated Leave", backdatedlbl.getText(),
				Matchers.containsString("Back Dated"));
		boolean verifyapplieddatelbl = Validator.verifyThat("Applied Date", applieddatelbl.getText(),
				Matchers.containsString("Applied"));
		boolean verifyleavedatelbl = Validator.verifyThat("Leave Date", leavedatelbl.getText(),
				Matchers.containsString("Leave"));


		soft.assertTrue(verifyenamelbl);
		soft.assertTrue(verifyeidlbl);
		soft.assertTrue(verifyetypelbl);
		soft.assertTrue(verifyldurationlbl);
		soft.assertTrue(verifystatuslbl);
		soft.assertTrue(verifyreasonlbl);
		soft.assertTrue(verifyprojectlbl);
		soft.assertTrue(verifymngrcmmntlbl);
		soft.assertTrue(verifyactionslbl);
		soft.assertTrue(verifybackdatedlbl);
		soft.assertTrue(verifyapplieddatelbl);
		soft.assertTrue(verifyleavedatelbl);
		soft.assertAll();

	}
	@QAFTestStep(description="user selects the first leave request and clicks on Approve")
	public void actionDropdownSelect()
	{
		click("leavelistpage.actions.drpdwn");
		click("leavelistpage.actions.drpdwnvalue(Approve)");
	}

	@QAFTestStep(description="user verifies submit & approve all button")
	public void verifyButtons()
	{
		CommonUtils.scrollUpToElement("leavelistpage.submit.button");
		QAFExtendedWebElement submitbtn = new QAFExtendedWebElement("leavelistpage.submit.button");
		QAFExtendedWebElement approveallbtn = new QAFExtendedWebElement("leavelistpage.approveall.button");
		boolean submitdisplayed = submitbtn.isDisplayed();
		boolean approvealldisplayed = approveallbtn.isDisplayed();
		Assert.assertTrue(submitdisplayed);
		Assert.assertTrue(approvealldisplayed);
	}
}
