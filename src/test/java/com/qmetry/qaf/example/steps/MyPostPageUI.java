package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.List;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
public class MyPostPageUI extends WebDriverTestBase{
private static String actualMyPostBreadCrumb="";
private static String expectedMyPostBreadCrumb="Home  / Publisher";
	public void myPostPageNavigation()
	{
		CommonUtils.clickUsingJavascript("mngr.switchview");
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.scrollUpToElement("mngr.publisher.link");
		CommonUtils.clickUsingJavascript("mngr.publisher.link");
		CommonUtils.scrollUpToElement("mngr.myposts.link");
		click("mngr.myposts.link");

	}

	public void getTitle()
	{
		QAFExtendedWebElement getTitle=new QAFExtendedWebElement("mngr.myposts.title.text");
		boolean verifyTitle = Validator.verifyThat("My Posts Title", getTitle.getText(), Matchers.containsString("My Posts"));
		Assert.assertTrue(verifyTitle);
	}

	public void getBreadCrumb()
	{
			List<QAFWebElement> breadCrumbElements = getDriver().findElements("mngr.myposts.breadcrumb.text");
			
			for(int i=0;i<breadCrumbElements.size();i++)
			{
				actualMyPostBreadCrumb=actualMyPostBreadCrumb+breadCrumbElements.get(i).getText();
			}
			Assert.assertEquals(actualMyPostBreadCrumb, expectedMyPostBreadCrumb);
	}
	
	public void verifyHideFilter()
	{
		boolean verifyVisibleHideFilter = verifyVisible("mngr.hidefilter.button");
		Assert.assertTrue(verifyVisibleHideFilter);
	}
	
	public void verifyActionLink()
	{
		boolean verifyVisibleEditAction = verifyVisible("mngr.editaction.btn");
		Assert.assertTrue(verifyVisibleEditAction);
		boolean verifyVisibleDeleteAction = verifyVisible("mngr.deleteaction.btn");
		Assert.assertTrue(verifyVisibleDeleteAction);
	}
	
	public void verifyPagination()
	{
		boolean verifyVisiblePagination = verifyVisible("mngr.pagination.link");
		Assert.assertTrue(verifyVisiblePagination);
	}
	
	public void verifyTableColumns()
	{
		QAFExtendedWebElement titleLabel=new QAFExtendedWebElement("mngr.title.label");
		QAFExtendedWebElement categoryLabel=new QAFExtendedWebElement("mngr.category.label");
		QAFExtendedWebElement postedOnLabel=new QAFExtendedWebElement("mngr.postedon.label");
		QAFExtendedWebElement actionLabel=new QAFExtendedWebElement("mngr.action.label");
		
		boolean verifytitleLabel = Validator.verifyThat("Title Column", titleLabel.getText(),Matchers.containsString("Title"));

		boolean verifycategoryLabel = Validator.verifyThat("Category Column", categoryLabel.getText(),Matchers.containsString("Category"));

		boolean verifytpostedOnLabel = Validator.verifyThat("Posted On Column", postedOnLabel.getText(),Matchers.containsString("Posted On"));

		boolean verifyactionLabel = Validator.verifyThat("Action Column", actionLabel.getText(),Matchers.containsString("Action"));
		SoftAssert soft=new SoftAssert();
		soft.assertTrue(verifytitleLabel);
		soft.assertTrue(verifycategoryLabel);
		soft.assertTrue(verifytpostedOnLabel);
		soft.assertTrue(verifyactionLabel);
	}
	public void verifyDateFormat()
	{
		QAFExtendedWebElement dateText=new QAFExtendedWebElement("mngr.createdondate.text");
		 String[] splitDate = dateText.getText().split("-");
		 String day = Integer.toString(splitDate[0].length());
		 String month = Integer.toString(splitDate[1].length());
		 String year = Integer.toString(splitDate[2].length());
		 boolean verifyDay = Validator.verifyThat("Check Date", day, Matchers.containsString("2"));

		 boolean verifyMonth = Validator.verifyThat("Check Month", month, Matchers.containsString("3"));

		 boolean verifyYear = Validator.verifyThat("Check Year", year, Matchers.containsString("4"));
		 Assert.assertTrue(verifyDay);
		 Assert.assertTrue(verifyMonth);
		 Assert.assertTrue(verifyYear);
	}
	
	public void verifyLabels()
	{
		QAFExtendedWebElement fromDatetext=new QAFExtendedWebElement("mngr.frmdate.textfield");

		QAFExtendedWebElement toDatetext=new QAFExtendedWebElement("mngr.todate.textfield");

		QAFExtendedWebElement keywordTextArea=new QAFExtendedWebElement("mngr.keyword.textfield");
		QAFExtendedWebElement categoryDropdown=new QAFExtendedWebElement("mngr.category.drpdown");
		
	
		boolean verifyVisibleFromDateLabel = verifyVisible("mngr.frmdate.label");

		boolean verifyVisibleToDateLabel=verifyVisible("mngr.todate.label");
	
		boolean verifyFromDatePicker = Validator.verifyThat("Check From Date Picker", fromDatetext.getAttribute("myvalue"),Matchers.containsString("toDate"));
		boolean verifyToDatePicker = Validator.verifyThat("Check To Date Picker", toDatetext.getAttribute("myvalue"),Matchers.containsString("toDate"));

		boolean verifyKeywordTextArea = Validator.verifyThat("Check Keyword Textarea", keywordTextArea.getAttribute("type"),Matchers.containsString("text"));
		boolean verifyCategoryDropdown = Validator.verifyThat("Check Category Dropdown", categoryDropdown.getAttribute("ng-click"),Matchers.containsString("toggleDropdown()"));
	
		boolean verifyVisibleSearchButton = verifyVisible("mngr.search.button");

		boolean verifyVisibleResetButton = verifyVisible("mngr.reset.button");
		
		Assert.assertTrue(verifyVisibleFromDateLabel);
		Assert.assertTrue(verifyVisibleToDateLabel);
		Assert.assertTrue(verifyFromDatePicker);
		Assert.assertTrue(verifyToDatePicker);
		Assert.assertTrue(verifyKeywordTextArea);
		Assert.assertTrue(verifyCategoryDropdown);
		
		Assert.assertTrue(verifyVisibleSearchButton);
		Assert.assertTrue(verifyVisibleResetButton);
		
	}
}
