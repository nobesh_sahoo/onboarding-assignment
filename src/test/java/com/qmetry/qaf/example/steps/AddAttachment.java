package com.qmetry.qaf.example.steps;
import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;

import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;


public class AddAttachment extends WebDriverTestBase {



	public void addAttachment() throws AWTException 
	{
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.scrollUpToElement("emp.reimbursement.link");
		CommonUtils.clickUsingJavascript("emp.reimbursement.link");
		CommonUtils.scrollUpToElement("emp.myexpenselist.link");
		CommonUtils.clickUsingJavascript("emp.myexpenselist.link");
		CommonUtils.clickUsingJavascript("emp.newexpense.btn");
		CommonUtils.clickUsingJavascript("emp.browsefiles.btn");
		Robot robo=new Robot();
		StringSelection str=new StringSelection("C:\\Users\\nobesh.sahoo\\Desktop\\Snapshots\\Login,Logout.PNG");
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(str, null);

		robo.keyPress(java.awt.event.KeyEvent.VK_CONTROL);
		robo.keyPress(java.awt.event.KeyEvent.VK_V);

		robo.keyRelease(java.awt.event.KeyEvent.VK_CONTROL);
		robo.keyRelease(java.awt.event.KeyEvent.VK_V);

		robo.keyPress(java.awt.event.KeyEvent.VK_ENTER);
		robo.keyRelease(java.awt.event.KeyEvent.VK_ENTER);
		boolean isFileUploaded = verifyPresent("emp.filepath.text");
		Assert.assertTrue(isFileUploaded);
		CommonUtils.logout();
	}
}
