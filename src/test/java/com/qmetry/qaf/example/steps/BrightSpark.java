package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.QAFTestBase;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;


public class BrightSpark extends WebDriverTestBase {

	private static List<String> upperlabeltexts=new ArrayList<String>();

	private static List<String> lowerlabeltexts=new ArrayList<String>();

	private static String nomineeName="amit";
	private static String award="Bright";
	
	@QAFTestStep(description = "user clicks on side menu list,R&R menu and Bright Spark menu and nominates")
	public void sideMenuClick()
	{
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.scrollUpToElement("RnR.menu");
		click("RnR.menu");
		click("nominate.menu");
		CommonUtils.scrollUpToElement("brightspark.menu");
		click("brightspark.menu");
		CommonUtils.clickUsingJavascript("template.img");
		click("nominee.select");
		CommonUtils.switchToActiveElement();
		sendKeys(nomineeName,"input.searchbox");
		click("value.select");
		click("project.select");
		CommonUtils.switchToActiveElement();
		sendKeys("nest" ,"input.searchbox");
		click("value.select");
		sendKeys("abc" ,"challenging.txtarea");
		sendKeys("abc" ,"solutions.txtarea");
		sendKeys("abc" ,"benefits.txtarea");
		sendKeys("abc" ,"rational.txtarea");
		QAFTestBase.pause(5000);
		CommonUtils.scrollUpToElement("post.btn");
		CommonUtils.clickUsingJavascript("post.btn");

	}
	@QAFTestStep(description = "user switches to manager view")
	public void switchToManagerView()
	{
		CommonUtils.clickUsingJavascript("mngr.switchview");
		QAFTestBase.pause(10000);
	}
	@QAFTestStep(description = "user clicks on side menu list, R&R menu and R&R request")
	public void managerRnRRequest()
	{
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.scrollUpToElement("mngrRnR.menu");
		CommonUtils.clickUsingJavascript("mngrRnR.menu");
		CommonUtils.clickUsingJavascript("RnRrequest.menu");
	}
	@QAFTestStep(description = "user verifies approve and reject button")
	public void rnRVerification()
	{
		QAFExtendedWebElement rnrClickMenu=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("mngr.brightsparkmenu.link"), nomineeName,award));
		rnrClickMenu.click();
		boolean apprv_btn = verifyEnabled("approve.btn");
		boolean reject_btn = verifyEnabled("reject.btn");
		Assert.assertTrue(apprv_btn);
		Assert.assertTrue(reject_btn);


	}
	@QAFTestStep(description = "user rejects the request and checks the status")
	public void rejectRequest()
	{
		CommonUtils.scrollUpToElement("reject.btn");
		click("reject.btn");
		click("mngr.comment.txtarea");
		sendKeys("Better Luck Next Time","mngr.comment.txtarea");
		click("mngr.submit.btn");
		QAFExtendedWebElement managerStatus=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("mngr.managerstatus.value"), nomineeName,award));
		boolean verifymngrstatus = Validator.verifyThat("Rejected", managerStatus.getText(),
				Matchers.containsString("Rejected"));
		Assert.assertTrue(verifymngrstatus);
	}
	@QAFTestStep(description = "user verifies the Manager and HR Status")
	public void statusVerification()
	{

		QAFExtendedWebElement mngrstatus=new QAFExtendedWebElement("mngr.mngrstatus.text");
		QAFExtendedWebElement hrstatus=new QAFExtendedWebElement("mngr.hrstatus.text");

		boolean verifymngrstatus = Validator.verifyThat("Pending", mngrstatus.getText(),
				Matchers.containsString("Pending"));
		boolean verifyhrstatus = Validator.verifyThat("Pending", hrstatus.getText(),
				Matchers.containsString("Pending"));
		Assert.assertTrue(verifymngrstatus);
		Assert.assertTrue(verifyhrstatus);


	}

	@QAFTestStep(description = "user approves the request by manager")
	public void mngrApproveRequest()
	{
		CommonUtils.scrollUpToElement("approve.btn");
		click("approve.btn");
		click("mngr.comment.txtarea");
		sendKeys("Approved","mngr.comment.txtarea");
		click("mngr.submit.btn");

	}
	@QAFTestStep(description = "user approves the request by hr")
	public void hrApproveRequest()
	{
		CommonUtils.scrollUpToElement("approve.btn");
		click("approve.btn");
		click("mngr.comment.txtarea");
		sendKeys("Approved","mngr.comment.txtarea");
		click("mngr.submit.btn");
	}
	@QAFTestStep(description = "user verifies no of blocks,manager status,hr status")
	public void blockVerification()
	{
		SoftAssert soft=new SoftAssert();
		upperlabeltexts.add("Nominee");

		upperlabeltexts.add("Project");

		upperlabeltexts.add("Reward Name");

		upperlabeltexts.add("Posted Date");

		upperlabeltexts.add("Nominated By");

		upperlabeltexts.add("Manager");

		upperlabeltexts.add("Manager Status");

		upperlabeltexts.add("HR Status");

		lowerlabeltexts.add("Challenging situations faced");

		lowerlabeltexts.add("Solutions provided/Situation handling");

		lowerlabeltexts.add("Benefits accrued");

		lowerlabeltexts.add("Rationale for Nomination");

		lowerlabeltexts.add("Managers Comments");

	

		QAFExtendedWebElement rnrClickMenu=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("mngr.brightsparkmenu.link"), nomineeName,award));
		rnrClickMenu.click();
		
		ArrayList<String> ai1= new ArrayList<String>();
		ArrayList<String> ai2= new ArrayList<String>();
		QAFTestBase.pause(5000);

		List<QAFWebElement> labelvalues1 = getDriver().findElements("noofblocks.value");
		List<QAFWebElement> labelvalues2 = getDriver().findElements("valueaddition.blocks.value");

		for(int i=0;i<labelvalues1.size();i++)
		{
			ai1.add(labelvalues1.get(i).getText());

		}
		for(int i=0;i<labelvalues2.size();i++)
		{
			ai2.add(labelvalues2.get(i).getText());

		}
		boolean equals = upperlabeltexts.equals(ai1);
		boolean equals2 = lowerlabeltexts.equals(ai2);
		soft.assertTrue(equals);
		soft.assertTrue(equals2);

		QAFExtendedWebElement managerstatustext =new QAFExtendedWebElement("mngr.mngrstatus.value");
		boolean verifymanagerstatustext = Validator.verifyThat("Approved", managerstatustext.getText(),
				Matchers.containsString("Approved"));
		QAFExtendedWebElement hrstatustext =new QAFExtendedWebElement("mngr.hrstatus.value");
		boolean verifyhrstatustext = Validator.verifyThat("Approved", hrstatustext.getText(),
				Matchers.containsString("Approved"));
		soft.assertTrue(verifymanagerstatustext);
		soft.assertTrue(verifyhrstatustext);
		soft.assertAll();
	}

	@QAFTestStep(description = "user clicks on the manager approved request")
	public void managerApprovedRequest()
	{

		QAFExtendedWebElement rnrClickMenu=new QAFExtendedWebElement(String.format(ConfigurationManager.getBundle().getString("mngr.brightsparkmenu.link"), nomineeName,award));
		rnrClickMenu.click();
	}

	@QAFTestStep(description = "user verifies back button,img display")
	public void otherVerification()
	{
		QAFExtendedWebElement rewardimg=new QAFExtendedWebElement("mngr.rwrd.img");
		boolean verifyimgdisplay = rewardimg.isDisplayed();
		Assert.assertTrue(verifyimgdisplay);
		QAFExtendedWebElement backbtn=new QAFExtendedWebElement("mngr.back.button");
		boolean verifybackbtn = backbtn.isDisplayed();
		Assert.assertTrue(verifybackbtn);



	}

}
