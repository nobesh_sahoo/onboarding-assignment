package com.qmetry.qaf.example.steps;


import org.testng.Assert;

import static com.qmetry.qaf.automation.step.CommonStep.*;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class PatOnBack extends WebDriverTestBase{

	public static void nomineeSelection()
	{
		click("side.menu");
		CommonUtils.scrollUpToElement("RnR.menu");
		click("RnR.menu");
		click("nominate.menu");
		CommonUtils.scrollUpToElement("patonback.menu");
		click("patonback.menu");
		click("patonbackimage.template");
		click("nominee.select");
		CommonUtils.switchToActiveElement();
		sendKeys("virat" ,"input.searchbox");
		click("value.select");
		click("project.select");
		CommonUtils.switchToActiveElement();
		sendKeys("nest" ,"input.searchbox");
		click("value.select");
		click("keycontribution.textarea");
		sendKeys("Thanks for your contribution","keycontribution.textarea");
		CommonUtils.scrollUpToElement("post.btn");
		CommonUtils.clickUsingJavascript("post.btn");
		CommonUtils.logout();
	}

	public static void managerAction()
	{
		CommonUtils.clickUsingJavascript("mngr.switchview");
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.scrollUpToElement("mngr.patonbackrnr.menu");
		click("mngr.patonbackrnr.menu");
		CommonUtils.scrollUpToElement("mngr.rnrrequest.menu");
		click("mngr.rnrrequest.menu");
		click("patonback.click.menu");
		boolean apprv_btn = verifyEnabled("approve.btn");
		boolean reject_btn = verifyEnabled("reject.btn");
		Assert.assertTrue(apprv_btn);
		Assert.assertTrue(reject_btn);
		CommonUtils.logout();

	}
	public static void deliveryManagerAction()
	{
		CommonUtils.clickUsingJavascript("mngr.switchview");
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.scrollUpToElement("hrmngr.rnr.menu");
		click("hrmngr.rnr.menu");
		CommonUtils.scrollUpToElement("hrmanager.rnrrequest.menu");
		click("hrmanager.rnrrequest.menu");
		click("patonback.click.menu");
		boolean apprv_btn = verifyEnabled("approve.btn");
		boolean reject_btn = verifyEnabled("reject.btn");
		Assert.assertTrue(apprv_btn);
		Assert.assertTrue(reject_btn);
	}



}
