package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;

import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.Random;

import org.hamcrest.Matchers;
import org.testng.Assert;

import com.qmetry.qaf.automation.core.QAFTestBase;

public class Request extends WebDriverTestBase {

	private static WebDriverTestBase wtb;
	@QAFTestStep(description = "user does TravelTest and logout")
	public  void travelTest() {

		CommonUtils.clickUsingJavascript("home.switchtomanager.btn");
		CommonUtils.clickUsingJavascript("home.manager.travel.link");
		mouseOver("manager.requestspy.btn");
		CommonUtils.clickUsingJavascript("manager.requestspy.btn");
		CommonUtils.scrollUpToElement("travel.approve.btn");
		Assert.assertTrue(verifyEnabled("travel.approve.btn"));
		Assert.assertTrue(verifyEnabled("travel.reject.btn"));
		Assert.assertTrue( verifyEnabled("travel.back.btn"));
		
			
		CommonUtils.logout();

	}
	@QAFTestStep(description = "user performs viewall activity")
	public  void testViewAll() {
		wtb = new WebDriverTestBase();

		QAFExtendedWebElement qafLeave = new QAFExtendedWebElement("leave.list.page.title");
		QAFExtendedWebElement qafTravel = new QAFExtendedWebElement("travel.list.page.title");
		QAFExtendedWebElement qafExpense = new QAFExtendedWebElement("expense.list.page.title");

		CommonUtils.clickUsingJavascript("home.switchtomanager.btn");
		CommonUtils.clickUsingJavascript("home.viewAll.tab");
		Validator.verifyThat("Team Leave List verification", qafLeave.getText(),
				Matchers.containsString("Team Leave List"));
		wtb.getDriver().navigate().back();
		QAFTestBase.pause(10000);
		CommonUtils.clickUsingJavascript("home.manager.travel.link");
		CommonUtils.clickUsingJavascript("home.viewAll.tab");
		Validator.verifyThat("Travel Requests verification", qafTravel.getText(),
				Matchers.containsString("Travel Requests"));
		wtb.getDriver().navigate().back();
		QAFTestBase.pause(10000);
		CommonUtils.clickUsingJavascript("home.manager.expense.link");
		CommonUtils.clickUsingJavascript("home.viewAll.tab");
		Validator.verifyThat("Team Reimbursement List verification", qafExpense.getText(),
				Matchers.containsString("Team Reimbursement List"));

	} 
	@QAFTestStep(description="user navigates to Expense Reimbursement -> My Expense List -> New Expense ->Enter mandatory details -> Click on Submit")
	public void raiseExpenseRequest()
	{
		click("home.emp.expenserequest.link");
		click("home.footer.viewall.link");
		click("expense.newexpense.btn");
		sendKeys("Test","expense.titlearea.input");
		click("expense.project.drpdown");
		click("expense.project.value");
		click("expense.category.drpdown");
		CommonUtils.scrollUpToElement("expense.category.drpdown.value");
		click("expense.category.drpdown.value");
		click("expense.amount.input");
		Random r=new Random();
		int price = r.nextInt(9999);
		String projectprice = Integer.toString(price);
		sendKeys(projectprice, "expense.amount.input");
		click("expense.submit.button");

	}
	@QAFTestStep(description="user verifies the page title")
	public void expenseRequestVerification()
	{	
		QAFExtendedWebElement qafTitleVerification = new QAFExtendedWebElement("expense.mylist.title");
		boolean verifyTitle = Validator.verifyThat("My Expense List", qafTitleVerification.getText(),Matchers.containsString("Expense List"));
		Assert.assertTrue(verifyTitle);

	}
	@QAFTestStep(description="user toggles to manager view->Expense Requests->View All->Enter approval comment->Approve")
	public void expenseRequestApproval()
	{
		click("home.switchtomanager.btn");
		click("manager.expenserequests.tab");
		click("manager.expenserequests.viewalltab");
		click("manager.expenserequest.titleclick");
		click("manager.mandatory.comment.textarea");
		click("manager.mandatory.comment.textarea");
		sendKeys("Approved","manager.mandatory.comment.textarea");
		CommonUtils.clickUsingJavascript("manager.expenserequest.approvebtn");

	}
}
