package com.qmetry.qaf.example.steps;


import org.testng.Assert;


import static com.qmetry.qaf.automation.step.CommonStep.*;


import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;



public class HomePage extends WebDriverTestBase   {


	@QAFTestStep(description = "user enters blank {0},{1}")
	public  void loginWOUnamePwd(String username, String password) {
		sendKeys(username, "uname.txtfield");
		sendKeys(password, "pwd.txtfield");
		click("login.btn");
		boolean verifyEnabled = verifyEnabled("errormsg.txt");
		Assert.assertTrue(verifyEnabled);

	}
	@QAFTestStep(description = "user enters {0},{1}")
	public  void login(String username, String password) {
		waitForPresent("uname.txtfield");
		sendKeys(username, "uname.txtfield");
		sendKeys(password, "pwd.txtfield");
		click("login.btn");
		boolean verifyEnabledtrainingcalender = verifyEnabled("unique.element.lbl");
		Assert.assertTrue(verifyEnabledtrainingcalender);
		
	}

	
	@QAFTestStep(description="user verifies if on homepage")
	public void homePageVerification()
	{
		boolean pwdrst_pnt = verifyEnabled("homepage.remeberpwd.lbl");
		Assert.assertTrue(pwdrst_pnt);
	}



}
