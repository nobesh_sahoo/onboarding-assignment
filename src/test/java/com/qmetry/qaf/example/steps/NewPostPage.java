package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;


import static com.qmetry.qaf.automation.step.CommonStep.*;

import java.util.ArrayList;
import java.util.List;

import org.testng.asserts.SoftAssert;


public class NewPostPage extends WebDriverTestBase {

	private SoftAssert soft=new SoftAssert();
	private static String actualbreadcrumb="";
	private static String expectedbreadcrumb="Home  / Publisher  / My Posts";
	ArrayList<String> ai= new ArrayList<String>();
	public void clickPublisherLink()
	{
		CommonUtils.clickUsingJavascript("mngr.switchview");
		waitForPresent("side.menu");
		CommonUtils.clickUsingJavascript("side.menu");
		waitForPresent("mngr.publisher.link");
		CommonUtils.scrollUpToElement("mngr.publisher.link");
		CommonUtils.clickUsingJavascript("mngr.publisher.link");
		waitForPresent("mngr.myposts.link");
		CommonUtils.scrollUpToElement("mngr.myposts.link");
		CommonUtils.clickUsingJavascript("mngr.myposts.link");
		CommonUtils.clickUsingJavascript("create.button");

	}

	public void verifyBreadCrumb()
	{
		waitForPresent("breadcrumb.text");
		List<QAFWebElement> breadcrumbelements = getDriver().findElements("breadcrumb.text");
			for(int i=0;i<breadcrumbelements.size();i++)
		{
			actualbreadcrumb =actualbreadcrumb+ breadcrumbelements.get(i).getText();
		}

		soft.assertEquals(actualbreadcrumb, expectedbreadcrumb);
		QAFExtendedWebElement titletextarea= new QAFExtendedWebElement("title.textarea");
		String titletypeattribute = titletextarea.getAttribute("type");

		soft.assertEquals("text", titletypeattribute);
		QAFExtendedWebElement posturltextarea= new QAFExtendedWebElement("posturl.textarea");
		String posturltypeattribute = posturltextarea.getAttribute("type");
		soft.assertEquals("url", posturltypeattribute);

		QAFExtendedWebElement imageurltextarea= new QAFExtendedWebElement("imageurl.textarea");
		String imageurltypeattribute = imageurltextarea.getAttribute("type");
		soft.assertEquals("url", imageurltypeattribute);

		QAFExtendedWebElement descriptiontextarea= new QAFExtendedWebElement("description.textarea");
		String descriptiontypeattribute = descriptiontextarea.getAttribute("placeholder");
		soft.assertEquals("Enter Description", descriptiontypeattribute);

		QAFExtendedWebElement locationdropdown= new QAFExtendedWebElement("location.dropdown");
		String locationdropdownattribute = locationdropdown.getAttribute("ng-click");
		soft.assertTrue(locationdropdownattribute.contains("Dropdown"));

		QAFExtendedWebElement categorydropdown= new QAFExtendedWebElement("category.dropdown");
		String categorydropdownattribute = categorydropdown.getAttribute("ng-click");
		soft.assertTrue(categorydropdownattribute.contains("Dropdown"));

		QAFExtendedWebElement submitbtn= new QAFExtendedWebElement("submit.btn");
		String submitbtntypeattribute = submitbtn.getAttribute("type");
		soft.assertEquals("submit", submitbtntypeattribute);

		QAFExtendedWebElement backbtn= new QAFExtendedWebElement("back.btn");
		String backbtntypeattribute = backbtn.getAttribute("type");
		soft.assertEquals("button", backbtntypeattribute);

		QAFExtendedWebElement titleasterisk= new QAFExtendedWebElement("title.asterisk");
		String titleasterisktext = titleasterisk.getText();
		soft.assertEquals("*", titleasterisktext);

		QAFExtendedWebElement posturlasterisk= new QAFExtendedWebElement("posturl.asterisk");
		String posturlasterisktext = posturlasterisk.getText();
		soft.assertEquals("*", posturlasterisktext);

		QAFExtendedWebElement locationasterisk= new QAFExtendedWebElement("location.asterisk");
		String locationasterisktext = locationasterisk.getText();
		soft.assertEquals("*", locationasterisktext);

		QAFExtendedWebElement categoryasterisk= new QAFExtendedWebElement("category.asterisk");
		String categoryasterisktext = categoryasterisk.getText();
		soft.assertEquals("*", categoryasterisktext);

		QAFExtendedWebElement descriptionasterisk= new QAFExtendedWebElement("description.asterisk");
		String descriptionasterisktext = descriptionasterisk.getText();
		soft.assertEquals("*", descriptionasterisktext);



		soft.assertAll();

	}
}
