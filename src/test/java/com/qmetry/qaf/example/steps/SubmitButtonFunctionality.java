package com.qmetry.qaf.example.steps;

import static com.qmetry.qaf.automation.step.CommonStep.click;

import org.hamcrest.Matchers;
import org.testng.Assert;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class SubmitButtonFunctionality extends WebDriverTestBase {

	
	public void functionality()
	{
		click("side.menu");
		click("emp.reimbursement.link");
		CommonUtils.scrollUpToElement("emp.myexpenselist.link");
		click("emp.myexpenselist.link");
		click("emp.newexpense.link");
		CommonUtils.clickUsingJavascript("emp.submitexpense.btn");
		QAFExtendedWebElement errmsgtxt=new QAFExtendedWebElement("emp.expenseerrormsg.txt");
		boolean verifyerrmsgtxt = Validator.verifyThat("Check Error Message", errmsgtxt.getText(), Matchers.containsString("required field"));
		Assert.assertTrue(verifyerrmsgtxt);
	}
}
