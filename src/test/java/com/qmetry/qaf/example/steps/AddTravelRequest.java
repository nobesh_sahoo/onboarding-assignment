package com.qmetry.qaf.example.steps;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;

import com.qmetry.qaf.automation.util.Validator;

import static com.qmetry.qaf.automation.step.CommonStep.*;



import org.hamcrest.Matchers;
import org.testng.asserts.SoftAssert;
public class AddTravelRequest extends WebDriverTestBase {

	
	public void verifyMyTravelRequest()
	{
		CommonUtils.clickUsingJavascript("side.menu");
		CommonUtils.clickUsingJavascript("emp.travel.link");
		CommonUtils.clickUsingJavascript("emp.mytravelrequest.link");
		click("emp.newtravelrequest.button");
	}
	
	public void verifyLabels()
	{
		SoftAssert soft=new SoftAssert();
		boolean verifyPresentPurposeOfTravel = verifyPresent("emp.purposeoftravel.label");
		soft.assertTrue(verifyPresentPurposeOfTravel);
		boolean verifyPresentType = verifyPresent("emp.type.label");
		soft.assertTrue(verifyPresentType);
		boolean verifyPresentTravelType = verifyPresent("emp.traveltype.label");
		soft.assertTrue(verifyPresentTravelType);
		boolean verifyPresentFromJourney = verifyPresent("emp.fromjourney.label");
		soft.assertTrue(verifyPresentFromJourney);
		boolean verifyPresentToJourney = verifyPresent("emp.tojourney.label");
		soft.assertTrue(verifyPresentToJourney);
		boolean verifyPresentJourneyStartDate = verifyPresent("emp.journeystartdate.label");
		soft.assertTrue(verifyPresentJourneyStartDate);
		boolean verifyPresentJourneyEndDate = verifyPresent("emp.journeyenddate.label");
		soft.assertTrue(verifyPresentJourneyEndDate);
		boolean verifyPresentClientProjectName = verifyPresent("emp.clientprojectname.label");
		soft.assertTrue(verifyPresentClientProjectName);
		boolean verifyPresentCabBooking = verifyPresent("emp.cabbooking.label");
		soft.assertTrue(verifyPresentCabBooking);
		boolean verifyPresentHotelBooking = verifyPresent("emp.hotelbooking.label");
		soft.assertTrue(verifyPresentHotelBooking);
		boolean verifyPresentCashAdvance = verifyPresent("emp.cashadvance.label");
		soft.assertTrue(verifyPresentCashAdvance);
		boolean verifyPresentExtraInformation = verifyPresent("emp.extrainformation.label");
		soft.assertTrue(verifyPresentExtraInformation);
		boolean verifyPresentIdProof = verifyPresent("emp.idproof.label");
		soft.assertTrue(verifyPresentIdProof);
		soft.assertAll();
	}
	
	public void verifyMandatoryFields()
	{
		SoftAssert soft=new SoftAssert();
		QAFExtendedWebElement qafPurposeOfTravelField=new QAFExtendedWebElement("emp.purposeoftravel.asterisk");
		boolean verifyPurposeOfTravelField= Validator.verifyThat("Check Purpose of Travel Field", qafPurposeOfTravelField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyPurposeOfTravelField);
		
		QAFExtendedWebElement qafTypeField=new QAFExtendedWebElement("emp.type.asterisk");
		boolean verifyTypeField= Validator.verifyThat("Check Type Field", qafTypeField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyTypeField);
		
		QAFExtendedWebElement qafFromJourneyField=new QAFExtendedWebElement("emp.fromjourney.asterisk");
		boolean verifyFromJourneyField= Validator.verifyThat("Check Journey From Field", qafFromJourneyField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyFromJourneyField);
		
		QAFExtendedWebElement qafToJourneyField=new QAFExtendedWebElement("emp.tojourney.asterisk");
		boolean verifyToJourneylField= Validator.verifyThat("Check Journey To Field", qafToJourneyField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyToJourneylField);
		
		QAFExtendedWebElement qafJourneyStartDateField=new QAFExtendedWebElement("emp.journeystartdate.asterisk");
		boolean verifyJourneyStartDateField= Validator.verifyThat("Check Journey Start Date Field", qafJourneyStartDateField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyJourneyStartDateField);
		
		QAFExtendedWebElement qafJourneyEndDatelField=new QAFExtendedWebElement("emp.journeyenddate.asterisk");
		boolean verifyJourneyEndDateField= Validator.verifyThat("Check Journey End Date Field", qafJourneyEndDatelField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyJourneyEndDateField);
		
		QAFExtendedWebElement qafClientProjectNameField=new QAFExtendedWebElement("emp.clientprojectname.asterisk");
		boolean verifyClientProjectNameField= Validator.verifyThat("Check Client Project Name Field", qafClientProjectNameField.getAttribute("class"), Matchers.containsString("astriksym1"));
		soft.assertTrue(verifyClientProjectNameField);
		
		soft.assertAll();
	
	}
}
