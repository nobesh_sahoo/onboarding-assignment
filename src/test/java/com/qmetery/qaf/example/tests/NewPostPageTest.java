package com.qmetery.qaf.example.tests;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;


import com.qmetry.qaf.example.steps.CommonUtils;
import com.qmetry.qaf.example.steps.NewPostPage;


public class NewPostPageTest extends BaseTest  {
	private static String path="C://Users//nobesh.sahoo//git//AutomationOnboarding//resources//data.properties";
	private static Properties prop;

	public NewPostPageTest() throws IOException 
	{
		prop=new Properties();
		FileInputStream fis=new FileInputStream(path);
		prop.load(fis);

	}
	@Test
	public void newPostPageVerification()
	{

		NewPostPage npp=new NewPostPage();
		String uname = prop.getProperty("hr_uname");
		String pwd = prop.getProperty("hr_pwd");
		CommonUtils.login(uname, pwd);
		npp.clickPublisherLink();
		npp.verifyBreadCrumb();
		CommonUtils.logout();
	}
	


}
