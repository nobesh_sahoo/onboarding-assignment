package com.qmetery.qaf.example.tests;

import java.awt.AWTException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;

import com.qmetry.qaf.example.steps.AddAttachment;
import com.qmetry.qaf.example.steps.CommonUtils;


public class AddAttachmentTest extends BaseTest  {
	private static String path="C://Users//nobesh.sahoo//git//AutomationOnboarding//resources//data.properties";
	private static Properties prop;
	public AddAttachmentTest() throws IOException 
	{
		prop=new Properties();
		FileInputStream fis=new FileInputStream(path);
		prop.load(fis);

	}
	
	@Test(priority=1)
	public void addAttachmentVerification() throws AWTException
	{
		AddAttachment am=new AddAttachment();
		String ash_uname = prop.getProperty("ashish_uname");
		String ash_pwd = prop.getProperty("ashish_pwd");
		CommonUtils.login(ash_uname, ash_pwd);
		am.addAttachment();
		
	}
}
