package com.qmetery.qaf.example.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;

import com.qmetry.qaf.example.steps.CommonUtils;
import com.qmetry.qaf.example.steps.MyPostPageUI;


public class MyPostPageUITest extends BaseTest {

	
	private static String path="C://Users//nobesh.sahoo//git//AutomationOnboarding//resources//data.properties";
	private static Properties prop;

	public MyPostPageUITest() throws IOException 
	{
		prop=new Properties();
		FileInputStream fis=new FileInputStream(path);
		prop.load(fis);

	}
	
	@Test
	public void uIVerification()
	{
		MyPostPageUI mpp=new MyPostPageUI();
		String gaurav_uname = prop.getProperty("hr_uname");
		String gaurav_pwd = prop.getProperty("hr_pwd");
		CommonUtils.login(gaurav_uname, gaurav_pwd);
		mpp.myPostPageNavigation();
		mpp.getTitle();
		mpp.getBreadCrumb();
		mpp.verifyHideFilter();
		mpp.verifyActionLink();
		mpp.verifyPagination();
		mpp.verifyTableColumns();
		mpp.verifyDateFormat();
		mpp.verifyLabels();
		CommonUtils.logout();
	}
	
}
