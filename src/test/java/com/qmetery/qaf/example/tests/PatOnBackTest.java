package com.qmetery.qaf.example.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;

import com.qmetry.qaf.example.steps.CommonUtils;
import com.qmetry.qaf.example.steps.PatOnBack;

public class PatOnBackTest extends BaseTest{

	private static String path="C://Users//nobesh.sahoo//git//AutomationOnboarding//resources//data.properties";
	private static Properties prop;
	
	public PatOnBackTest() throws IOException 
	{
		prop=new Properties();
		FileInputStream fis=new FileInputStream(path);
		prop.load(fis);

	}

	@Test
	public void patOnBackApprovalProcess()
	{
		//PatOnBack pb=new PatOnBack();
		String ash_uname = prop.getProperty("ashish_uname");
		String ash_pwd = prop.getProperty("ashish_pwd");
		String tushit_uname = prop.getProperty("mngr_uname");
		String tushit_pwd = prop.getProperty("mngr_pwd");
		CommonUtils.login(ash_uname, ash_pwd);
		PatOnBack.nomineeSelection();
		CommonUtils.login(tushit_uname, tushit_pwd);
		PatOnBack.managerAction();
		String keshav_uname = prop.getProperty("dm_uname");
		String keshav_pwd = prop.getProperty("dm_pwd");
		CommonUtils.login(keshav_uname, keshav_pwd);
		PatOnBack.deliveryManagerAction();
		CommonUtils.logout();
	}
	
	
}
