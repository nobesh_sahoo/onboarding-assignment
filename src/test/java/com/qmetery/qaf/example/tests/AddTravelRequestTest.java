package com.qmetery.qaf.example.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.Test;

import com.qmetry.qaf.example.steps.AddTravelRequest;
import com.qmetry.qaf.example.steps.CommonUtils;

public class AddTravelRequestTest extends BaseTest{
	private static String path="C://Users//nobesh.sahoo//git//AutomationOnboarding//resources//data.properties";
	private static Properties prop;
	public AddTravelRequestTest() throws IOException 
	{
		prop=new Properties();
		FileInputStream fis=new FileInputStream(path);
		prop.load(fis);

	}
	
	@Test
	public void checkAddTravelRequest()
	{
		AddTravelRequest atr=new AddTravelRequest();
		String ash_uname = prop.getProperty("ashish_uname");
		String ash_pwd = prop.getProperty("ashish_pwd");
		CommonUtils.login(ash_uname, ash_pwd);
		atr.verifyMyTravelRequest();
		atr.verifyLabels();
		atr.verifyMandatoryFields();
		CommonUtils.logout();
	}
}
